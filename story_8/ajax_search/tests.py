from django.test import TestCase
from django.urls import reverse
from django.test import Client

class UnitTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.response = Client().get(reverse("ajax_search:index"))

    def test_root_url_status_200(self):
        self.assertEqual(self.response.status_code, 200)

    def test_correct_template_is_used(self):
        self.assertTemplateUsed(self.response, "ajax_search/index.html")
        