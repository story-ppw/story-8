// Function for making a delay
function delay(callback, ms) {
    let timer = 0;
    return function() {
        let context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function() {
            callback.apply(context, args);
        }, ms || 0);
    };
}

let typed = "";
let index = 0;
let max_index = 0;

$(function() {
    // Detect when enter is pressed and focuses to the input form
    $(document).keypress(function(e){
        if(e.key == 'Enter'){
            $("#search").focus();
        }
    });

    // Detect when there's a change on input
    $("#search").on("input", delay(function() {
        typed = $(this).val();
        index = 0;

        $.ajax({
            url: "/",
            data: {"input": typed, "startIndex": index},
            success: (data) => {
                max_index = data.totalItems;
                $("#results").empty();
                index = 0;
                if (max_index != 0) {
                    $("#empty-result").hide();
                    $.each(data.items, function(key, value) {
                        let title = value.volumeInfo.title;
                        let image_url = (value.volumeInfo.imageLinks ? value.volumeInfo.imageLinks.thumbnail : "https://via.placeholder.com/1024x1024.png?text=NO+IMAGE+AVAILABLE");
                        let authors = (value.volumeInfo.authors ? value.volumeInfo.authors.join(", ") : "-");
                        let buy = value.volumeInfo.infoLink;
                        let preview = value.volumeInfo.previewLink;

                        // Create new accordion item with template
                        let clone = $("#result-template").contents().clone(true);
                        clone.find("header").text(title);
                        clone.find("img").attr("src", image_url);
                        clone.find("h5").text("Author: " + authors);
                        clone.find("#buy").attr("href", buy);
                        clone.find("#preview").attr("href", preview);
                        $( "#results" ).append(clone).show("slow");
                    });
                    index += 10;
                } else {
                    $("#empty-result").show();
                }
            },
            failure: () => {
                alert("There is a failure");
            },
        })
    }, 500));
    
    // Infinite scrolling
    $(window).scroll(function() {
        // End of the document reached
        if ($(document).height() - $(this).height() == $(this).scrollTop() && index <= max_index) {
            $.ajax({
                url: "/",
                data: {"input": typed, "startIndex": index},
                success: (data) => {
                    $.each(data.items, function(key, value) {
                        let title = value.volumeInfo.title;
                        let image_url = (value.volumeInfo.imageLinks ? value.volumeInfo.imageLinks.thumbnail : "https://via.placeholder.com/1024x1024.png?text=NO+IMAGE+AVAILABLE");
                        let authors = (value.volumeInfo.authors ? value.volumeInfo.authors.join(", ") : "-");

                        // Create new accordion item with template
                        let clone = $("#result-template").contents().clone(true);
                        clone.find("header").text(title);
                        clone.find("img").attr("src", image_url);
                        clone.find("h5").text("Author: " + authors);
                        clone.find("#buy").attr("href", buy);
                        clone.find("#preview").attr("href", preview);
                        $( "#results" ).append(clone);
                    });
                    index += 10;
                },
                failure: () => {
                    alert("There is a failure");
                },
            });
        }
    }); 
})