from django.urls import path

from .views import index

app_name = 'ajax_search'

urlpatterns = [
    path('', index, name='index'),
]
