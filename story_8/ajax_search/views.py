from django.shortcuts import render
import requests
from django.http import JsonResponse

def index(request):
    input = request.GET.get('input')
    index = request.GET.get('startIndex')

    if request.method == "GET" and (input != None):
        url = f"https://www.googleapis.com/books/v1/volumes?q={input}&startIndex={index}"
        response = requests.get(url).json()
        return JsonResponse(response)
    return render(request, 'ajax_search/index.html')
