# Story 8

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

A simple search box using AJaX

[pipeline-badge]: https://gitlab.com/story-ppw/story-8/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/story-ppw/story-8/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/story-ppw/story-8/-/commits/master
